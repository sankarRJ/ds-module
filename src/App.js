
import './App.css';
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import { Grid } from '@mui/material';
import './Css/Media.css';
import Header from "./Components/Header/Header"
import Table from "./Components/Table/Tables"
import Login from './Components/Form/Login';
import Card from './Components/Cards/Cards';
import Grids from "./Components/Grid/Grids"
function App() {
  return (
    <div className="App">
      <Router>
        <Routes>
          <Route path="/" element={<Header />} />
          <Route path='/Table' element={<Table />} />
          <Route path='/Login' element={<Login />} />
          <Route path='/Card' element={<Card />} />
          <Route path="/Grid" element={<Grids />} />
        </Routes>
      </Router>

    </div>
  );
}

export default App;
