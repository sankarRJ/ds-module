import * as React from 'react';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import { CardActionArea } from '@mui/material';
import img from "../../Img/wall.jpg"
import Grid from '@mui/material/Grid';
import Header from '../Header/Header';
import "../../Css/Card.css";
import {Button} from '@mui/material';


const rows = [
  {img:`${img}`,T1:"Messi",Des:"WorldCup winner 2022"},
  {img:`${img}`,T1:"Messi",Des:"WorldCup winner 2022"},
  {img:`${img}`,T1:"Messi",Des:"WorldCup winner 2022"},
  {img:`${img}`,T1:"Messi",Des:"WorldCup winner 2022"},
  {img:`${img}`,T1:"Messi",Des:"WorldCup winner 2022"},
  {img:`${img}`,T1:"Messi",Des:"WorldCup winner 2022"},
  {img:`${img}`,T1:"Messi",Des:"WorldCup winner 2022"},
  {img:`${img}`,T1:"Messi",Des:"WorldCup winner 2022"},
  {img:`${img}`,T1:"Messi",Des:"WorldCup winner 2022"},
  {img:`${img}`,T1:"Messi",Des:"WorldCup winner 2022"},
  {img:`${img}`,T1:"Messi",Des:"WorldCup winner 2022"},
  {img:`${img}`,T1:"Messi",Des:"WorldCup winner 2022"},
  {img:`${img}`,T1:"Messi",Des:"WorldCup winner 2022"},
  {img:`${img}`,T1:"Messi",Des:"WorldCup winner 2022"},
  {img:`${img}`,T1:"Messi",Des:"WorldCup winner 2022"},
  {img:`${img}`,T1:"Messi",Des:"WorldCup winner 2022"},
  {img:`${img}`,T1:"Messi",Des:"WorldCup winner 2022"},
  {img:`${img}`,T1:"Messi",Des:"WorldCup winner 2022"},
  {img:`${img}`,T1:"Messi",Des:"WorldCup winner 2022"},
]

export default function Cards() {
    return (
        <div className='Cards'>
                <Grid container spacing={1}>
        <Grid item xs={12} sm={12} md={12} lg={12} xl={12}>
            <Header />
            </Grid>
            
            <Grid container spacing={2} >
              {rows.map((row,ind)=>{
                return(
                  <>
                    <Grid item xs={12} sm={12} md={1} lg={3} xl={2}>
            <Card sx={{ maxWidth: 320 }}>
        <CardActionArea key={ind}>
          <CardMedia
            component="img"
            height="140"
            image={row.img}
            alt="green iguana"
          />
          <CardContent className='card-content-div'>
            <div>
    <div>
              {row.T1}
            </div>
              <div>
              {row.T1}
            </div>
            </div>
            <div>

            </div>
          <div>

          </div>
          <div>
            <div>

            </div>
            <div>

            </div>
          </div>
          <div>
            <Button>
              
            </Button>
          </div>
        
          </CardContent>
        </CardActionArea>
      </Card>
            </Grid>
                  </>
                )
              })}
          
            </Grid>
            </Grid>
        </div>
      
    );
  }